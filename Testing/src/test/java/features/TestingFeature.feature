Feature: Test

  Scenario: User signs up
    When user navigates to "Main Page" Page
    And user clicks on "Sign Up Button" button
    Then user is taken to "Sign Up Page" Page
    When user enters "Test" value in "Name Input" field
    And user enters "random1@mail.ru" value in "Email Input" field
    And user enters "123456" value in "Password Input" field
    And user enters "123456" value in "Password Confirmation Input" field
    And user clicks on "Sign Up Registration Button" button
    Then user is taken to "Pricing Page" Page
    And user clicks on "Starter Subscribe Button" button
    And user is taken to "Subscribe Page" Page
#    And user switches to iframe
#    And user enters "4242424242`````````````424242" value in "Card Field" field
#    And user enters "1223" value in "Card Month" field
#    And user enters "123" value in "Card CVC" field
#    And user enters "55555" value in "Card Index" field
#    And user clicks on "Submit Button" button
#    Then user is taken to "Main Page" Page


  Scenario: User signs in
    When user navigates to "Main Page" Page
    And user clicks on "Sign In Button" button
    Then user is taken to "Sign In Page" Page
    And user enters "random1@mail.ru" value in "Email Input" field
    And user enters "123456" value in "Password Input" field
    And user clicks on "Log In Button" button
    And user is taken to "Main Page" Page


  Scenario: User adds Book to Library
    When user navigates to "Main Page" Page
    And user clicks on "Sign In Button" button
    Then user is taken to "Sign In Page" Page
    And user enters "test@mail.ru" value in "Email Input" field
    And user enters "123456" value in "Password Input" field
    And user clicks on "Log In Button" button
    And user is taken to "Library Page" Page
    And user buys first book
    Then user is taken to "My Library Page" Page
    And book is in user library

  Scenario: User deletes Book from Library
    When user navigates to "Main Page" Page
    And user clicks on "Sign In Button" button
    Then user is taken to "Sign In Page" Page
    And user enters "test@mail.ru" value in "Email Input" field
    And user enters "123456" value in "Password Input" field
    And user clicks on "Log In Button" button
    And user is taken to "Library Page" Page
    And user clicks on "My Library Button" button
    Then user is taken to "My Library Page" Page
    Then user deletes first book
    And user is taken to "Library Page" Page
    And user clicks on "My Library Button" button
    Then user is taken to "My Library Page" Page
    And "Empty Library" is displayed


  Scenario: User deletes his account
    When user navigates to "Main Page" Page
    And user clicks on "Sign In Button" button
    Then user is taken to "Sign In Page" Page
    And user enters "random1@mail.ru" value in "Email Input" field
    And user enters "123456" value in "Password Input" field
    And user clicks on "Log In Button" button
    And user is taken to "Main Page" Page
    And user clicks on "Account Button" button
    And user is taken to "Account Page" Page
    And user clicks on "Cancel my Account" button
    And user accepts alert
    Then user is taken to "Main Page" Page









