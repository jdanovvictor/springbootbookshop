package steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Driver {
    WebDriver driver;

    public Driver() {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\geckodriver.exe");
        driver = new FirefoxDriver();
    }

    public WebDriver getDriver() {
        if (driver == null) {
            new Driver();
        }
        return driver;
    }

}

