package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import steps.ScenarioContext;


public class BaseHook {

    @Autowired
    static ScenarioContext scenarioContext;

    Driver driver = new Driver();

    public static ScenarioContext getScenarioContext() {
        return scenarioContext;
    }

    @Before(order = 1)
    public void createContext() {
    scenarioContext = new ScenarioContext();
    }

    @Before(order = 2)
    public void initDriver() {
        WebDriver webDriver = driver.getDriver();
        scenarioContext.createEnviroment(webDriver);
    }

    @After()
    public void closeDriver() {
        scenarioContext.closeBrowser();
    }
}

