package steps;

import PageObjects.BookModule;
import PageObjects.LibraryPage;
import PageObjects.MyLibraryPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.testmonkeys.maui.core.browser.popups.BrowserPopUps;
import org.testmonkeys.maui.pageobjects.AbstractPage;
import org.testmonkeys.maui.pageobjects.elements.AbstractComponent;
import org.testmonkeys.maui.pageobjects.elements.Input;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class Steps {


    @Autowired
    ScenarioContext scenarioContext = BaseHook.getScenarioContext();

    @When("user navigates to {string} Page")
    public void userNavigatesToPage(String pageName) {
        scenarioContext.setContext("currentPage", goToPage(pageName));
    }

    @And("user clicks on {string} button")
    public void userClicksOnButton(String elementName) {
        AbstractPage page = (AbstractPage) scenarioContext.getContext("currentPage");
        AbstractComponent abstractComponent = scenarioContext.getUiEnviroment().getPageScanner().findPageElementByName(page,elementName);
        abstractComponent.find().click();
    }

    @Then("user is taken to {string} Page")
    public void userIsTakenToPage(String pageName) {
        AbstractPage page = (AbstractPage)scenarioContext.getUiEnviroment().getPageFactory().createPage(pageName);
        scenarioContext.setContext("currentPage", page);
    }

    public AbstractPage goToPage(String pageName) {
        AbstractPage page = (AbstractPage) scenarioContext.getUiEnviroment().getPageFactory().createPage(pageName);
        page.open();
        return page;
    }

    @When("user enters {string} value in {string} field")
    public void userEntersValueInField(String value, String field) {
        AbstractPage page = (AbstractPage) scenarioContext.getContext("currentPage");
        Input input = scenarioContext.getUiEnviroment().getPageScanner().findPageElementByName(page,field);
        input.type(value);
    }


    @And("{string} is displayed")
    public void isDisplayed(String elementName) {
        AbstractPage page = (AbstractPage) scenarioContext.getContext("currentPage");
        AbstractComponent abstractComponent = scenarioContext.getUiEnviroment().getPageScanner().findPageElementByName(page,elementName);
        WebDriverWait wait = new WebDriverWait(scenarioContext.getUiEnviroment().getBrowser().getDriver(), 5);
        wait.until(ExpectedConditions.visibilityOf(abstractComponent.find()));
        assertThat(abstractComponent.isDisplayed()).isTrue();
    }

    @When("user accepts alert")
    public void userAcceptsAlert(){
        BrowserPopUps browserPopUps = new BrowserPopUps(scenarioContext.getUiEnviroment().getBrowser());
        browserPopUps.getAlert().accept();
    }

    @And("user switches to iframe")
    public void userSwitchesToIframe() {
        scenarioContext.getUiEnviroment().getBrowser().getDriver().switchTo().frame(0);

    }

    @And("user buys first book")
    public void userBuysFirstBook() {
        LibraryPage page = (LibraryPage) scenarioContext.getContext("currentPage");
        page.getBookList().get(0).getAddToLibraryButton().click();
        scenarioContext.setContext("selectedBook", page.getBookList().get(0).getTitle().getText());
    }

    @And("book is in user library")
    public void bookIsInUserLibrary() {
        MyLibraryPage page = (MyLibraryPage) scenarioContext.getContext("currentPage");
        List<BookModule> books= page.getBookList().getAll();
        assertThat(books.stream().findAny().filter(o-> o.getTitle().getText().equals(scenarioContext.getContext("selectedBook"))).isPresent()).isTrue();
    }

    @Then("user deletes first book")
    public void userDeletesFirstBook() {
        MyLibraryPage page = (MyLibraryPage) scenarioContext.getContext("currentPage");
        scenarioContext.setContext("selectedBook", page.getBookList().get(0).getTitle());
        page.getBookList().get(0).getRemoveFromLibraryButton().click();
    }
}
