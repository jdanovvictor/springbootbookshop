package steps;

import lombok.Getter;
import org.testmonkeys.maui.core.browser.Browser;
import org.testmonkeys.maui.core.factory.PageFactory;
import org.testmonkeys.maui.core.factory.PageScanner;

@Getter
public class UiEnviroment {

    Browser browser;
    PageScanner pageScanner;
    PageFactory pageFactory;

    public UiEnviroment(Browser browser, PageScanner pageScanner, PageFactory pageFactory) {
        this.browser = browser;
        this.pageScanner = pageScanner;
        this.pageFactory = pageFactory;

    }
}
