package steps;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;
import org.testmonkeys.maui.core.browser.Browser;
import org.testmonkeys.maui.core.factory.PageFactory;
import org.testmonkeys.maui.core.factory.PageScanner;

@Getter
@Setter
@Component
public class ScenarioContext{

    private UiEnviroment uiEnviroment;

    private  Map<String, Object> scenarioContext;

    public ScenarioContext(){
        scenarioContext = new HashMap<>();
    }

    public void setContext(Object key, Object value) {
        scenarioContext.put(key.toString(), value);
    }

    public Object getContext(Object key){
        return scenarioContext.get(key.toString());
    }

    public Boolean isContains(Object key){
        return scenarioContext.containsKey(key.toString());
    }

    public void createEnviroment(WebDriver driver){
        Browser browser = new Browser(driver);
        PageScanner pageScanner = new PageScanner("PageObjects");
        PageFactory pageFactory = new PageFactory(browser, pageScanner,  "https://bookshop.ironshieldmd.com/");
        this.uiEnviroment = new UiEnviroment(browser, pageScanner, pageFactory);
    }

    public void closeBrowser(){
        uiEnviroment.getBrowser().getDriver().quit();

    }
}