package PageObjects;

import lombok.Getter;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.elements.Button;
import org.testmonkeys.maui.pageobjects.elements.Label;
import org.testmonkeys.maui.pageobjects.modules.AbstractModule;

@Getter
public class BookModule extends AbstractModule {

    @ElementAccessor(elementName = "Title", byXPath = "//p[@class='title is-4']")
    private Label title;

    @ElementAccessor(elementName = "Author", byXPath = "//p[@class='subtitle is-6']")
    private Label Author;

    @ElementAccessor(elementName = "Add to Library Button", byXPath = "//a[text()='Add to library']")
    private Button addToLibraryButton;

    @ElementAccessor(elementName = "Remove from Library Button", byXPath = "//a[text()='Remove from library']")
    private Button removeFromLibraryButton;

}
