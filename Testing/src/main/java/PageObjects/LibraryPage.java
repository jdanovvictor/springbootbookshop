package PageObjects;


import lombok.Getter;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;
import org.testmonkeys.maui.pageobjects.elements.Button;
import org.testmonkeys.maui.pageobjects.elements.GroupComponent;

@Getter
@PageAccessor(name = "Library Page", url = "")
public class LibraryPage extends Page{

    @ElementAccessor(elementName = "Book List", byXPath = "//div[@class='book']")
    GroupComponent<BookModule> bookList;

    @ElementAccessor(elementName = "My Library Button", byXPath = "//a//i")
    private Button myLibraryButton;
}
