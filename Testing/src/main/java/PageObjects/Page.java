package PageObjects;

import lombok.Getter;
import org.testmonkeys.maui.pageobjects.AbstractPage;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;
import org.testmonkeys.maui.pageobjects.elements.Button;

@PageAccessor(name = "Page", url = "")
@Getter
public class Page extends AbstractPage {
    @ElementAccessor(elementName = "Menu Pricing Button", byXPath = "//*[text()='Pricing']")
    private Button menuPricingButton;

    @ElementAccessor(elementName = "Sign In Button", byXPath = "//*[text()='Sign In']")
    private Button menuSignInButton;

    @ElementAccessor(elementName = "Sign Up Button", byXPath = "//*[text()='Sign up']")
    private Button menuSignUpButton;

    @ElementAccessor(elementName = "Account Button", byXPath = "//*[text()='Account']")
    private Button accountButton;
}
