package PageObjects;

import lombok.Getter;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;
import org.testmonkeys.maui.pageobjects.elements.Button;

import java.awt.*;

@Getter
@PageAccessor(name = "Pricing Page", url = "/pricing")
public class PricingPage extends Page{

    @ElementAccessor(elementName = "Starter Subscribe Button", byXPath = "//a")
    private Button starterButton;

    @ElementAccessor(elementName = "Book Worm Subscribe Button", byXPath = "//a//following::a")
    private Button bookWormSubscribeButton;

    @ElementAccessor(elementName = "Scholar Subscribe Button", byXPath = "//a//following::a//following::a")
    private Button scholarSubscribeButton;

    @ElementAccessor(elementName = "Successful Registration Message", byXPath = "//*[text()='Welcome! You have signed up successfully.']")
    private Label successfulLoginMessage;
}
