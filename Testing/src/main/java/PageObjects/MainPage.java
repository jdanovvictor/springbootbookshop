package PageObjects;


import lombok.Getter;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;

import java.awt.*;

@PageAccessor(name = "Main Page", url = "")
@Getter
public class MainPage extends Page{

    @ElementAccessor(elementName = "Successful Login Message", byXPath = "//*[text()='Signed in successfully.']")
    private Label successfulLoginMessage;
}
