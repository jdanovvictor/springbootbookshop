package PageObjects;

import lombok.Getter;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;
import org.testmonkeys.maui.pageobjects.elements.Button;
import org.testmonkeys.maui.pageobjects.elements.Input;

@Getter
@PageAccessor(name = "Sign In Page", url = "/users/sign_in")
public class SignInPage extends Page{

    @ElementAccessor(elementName = "Email Input", byXPath = "//*[@id= 'user_email']")
    private Input emailInput;

    @ElementAccessor(elementName = "Password Input", byXPath = "//*[@id= 'user_password']")
    private Input passwordInput;

    @ElementAccessor(elementName = "Log In Button", byXPath = "//*[@value= 'Log in']")
    private Button logInButton;
}
