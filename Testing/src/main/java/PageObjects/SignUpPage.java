package PageObjects;


import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;
import org.testmonkeys.maui.pageobjects.elements.Button;
import org.testmonkeys.maui.pageobjects.elements.Input;

@PageAccessor(name = "Sign Up Page", url = "users/sign_up")
public class SignUpPage extends Page{

    @ElementAccessor(elementName = "Name Input", byXPath = "//*[@id='user_name']")
    private Input nameInput;

    @ElementAccessor(elementName = "Email Input", byXPath = "//*[@id='user_email']")
    private Input emailInput;

    @ElementAccessor(elementName = "Password Input", byXPath = "//*[@id='user_password']")
    private Input passwordInput;

    @ElementAccessor(elementName = "Password Confirmation Input", byXPath = "//*[@id='user_password_confirmation']")
    private Input passwordConfirmationInput;

    @ElementAccessor(elementName = "Sign Up Registration Button", byXPath = "//*[@value='Sign up']")
    private Button signUpButton;

    @ElementAccessor(elementName = "Log In Button", byXPath = "//*[text()='Log in']")
    private Button loginButton;
}
