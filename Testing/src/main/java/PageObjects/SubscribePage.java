package PageObjects;


import lombok.Getter;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;
import org.testmonkeys.maui.pageobjects.elements.AbstractComponent;
import org.testmonkeys.maui.pageobjects.elements.Button;
import org.testmonkeys.maui.pageobjects.elements.Input;

@Getter
@PageAccessor(name = "Subscribe Page", url = "/subscriptions")
public class SubscribePage extends Page{

    @ElementAccessor(elementName = "Card Field", byXPath = "//input[@placeholder='Номер карты']")
    private Input cardField;

    @ElementAccessor(elementName = "Card Month", byXPath = "//input[@placeholder='ММ / ГГ']")
    private Input cardMonth;

    @ElementAccessor(elementName = "Card CVC", byXPath = "//input[@placeholder='CVC']")
    private Input cardCVC;

    @ElementAccessor(elementName = "Card Index", byXPath = "//input[@placeholder='Индекс']")
    private Input cardIndex;

    @ElementAccessor(elementName = "Submit Button", byXPath = "//button[text()='Submit']")
    private Button submitButton;
}
