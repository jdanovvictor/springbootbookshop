package PageObjects;

import lombok.Getter;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;
import org.testmonkeys.maui.pageobjects.elements.Button;

@Getter
@PageAccessor(name = "Account Page", url = "/users/edit")
public class AccountPage extends Page{

    @ElementAccessor(elementName = "Cancel my Account", byXPath = "//a[text()='Cancel my account']")
    private Button cancelMyAccountButton;


}
