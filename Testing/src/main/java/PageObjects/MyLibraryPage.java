package PageObjects;

import lombok.Getter;
import org.testmonkeys.maui.pageobjects.ElementAccessor;
import org.testmonkeys.maui.pageobjects.PageAccessor;
import org.testmonkeys.maui.pageobjects.elements.GroupComponent;
import org.testmonkeys.maui.pageobjects.elements.Label;

@Getter
@PageAccessor(name = "My Library Page", url = "/library")
public class MyLibraryPage extends Page {

    @ElementAccessor(elementName = "Book List", byXPath = "//div[@class='book']")
    GroupComponent<BookModule> bookList;

    @ElementAccessor(elementName = "Empty Library", byXPath = "//div[text() = \"You haven't added any books to your library yet. \"]")
    Label emptyLibrary;
}
